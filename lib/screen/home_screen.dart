import 'package:flutter/material.dart';
import 'package:tomisha_work/screen/widgets/bottom_bar.dart';
import 'package:tomisha_work/screen/widgets/linear_line.dart';
import 'package:tomisha_work/screen/widgets/login_bar.dart';
import 'package:tomisha_work/screen/widgets/widgets.dart';
import 'package:tomisha_work/utils/app_size.dart';
import 'package:tomisha_work/utils/color_constants.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    bool isMobile = ScreenUtils.screenWidth(context) < 600;
    bool isWeb = ScreenUtils.screenWidth(context) > 600;

    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Column(
                children: [
                  LinearLine(
                    rightColor: AppColor.commonGreen,
                    leftColor: AppColor.linearBlue,
                  ),
                  LoginBar(),
                ],
              ),
              const ScrollingContent(),
              isMobile ? const BottomBar() : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
