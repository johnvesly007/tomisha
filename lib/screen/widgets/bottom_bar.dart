import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomisha_work/screen/widgets/register_btn.dart';
import 'package:tomisha_work/utils/app_size.dart';
import 'package:tomisha_work/utils/color_constants.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        boxShadow: [
          BoxShadow(
            color: AppColor.borderColor.withOpacity(0.3), // Shadow color
            spreadRadius: 0,
            blurRadius: 6,
            offset: const Offset(0, -3),
          ),
        ],
      ),
      height: ScreenUtils.getResponsiveHeight(
          context: context, portionHeightValue: 120),
      width: double.infinity,
      child: Column(children: [
        ScreenUtils.heightSpace(24, context),
        Row(
          children: [
            ScreenUtils.widthSpace(20, context),
            Expanded(child: const RegisterButton()),
            ScreenUtils.widthSpace(20, context),
          ],
        ),
        Expanded(child: Container())
      ]),
    );
  }
}
