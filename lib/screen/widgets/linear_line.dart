import 'package:flutter/material.dart';
import 'package:tomisha_work/utils/app_size.dart';

class LinearLine extends StatelessWidget {
  final Color rightColor;
  final Color leftColor;
  const LinearLine({
    super.key,
    required this.rightColor,
    required this.leftColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [rightColor, leftColor]),
      ),
      height: ScreenUtils.getResponsiveHeight(
          context: context, portionHeightValue: 5),
      width: double.infinity,
    );
  }
}
