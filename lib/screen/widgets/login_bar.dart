import 'package:flutter/material.dart';
import 'package:tomisha_work/utils/app_size.dart';
import 'package:tomisha_work/utils/color_constants.dart';

class LoginBar extends StatelessWidget {
  const LoginBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(12.0),
          bottomRight: Radius.circular(12.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5), // Shadow color
            spreadRadius: 1,
            blurRadius: 6,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      height: ScreenUtils.getResponsiveHeight(
          context: context, portionHeightValue: 67),
      width: double.infinity,
      child: Column(children: [
        ScreenUtils.heightSpace(26, context),
        Container(
          height: ScreenUtils.getResponsiveHeight(
              context: context, portionHeightValue: 21),
          child: Row(
            children: [
              Expanded(child: Container()),
              const Padding(
                padding: EdgeInsets.only(right: 26),
                child: Text(
                  "Login",
                  style: TextStyle(
                      fontSize: 14,
                      color: AppColor.commonGreen,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Lato"),
                ),
              ),
            ],
          ),
        ),
        Expanded(child: Container())
      ]),
    );
  }
}
