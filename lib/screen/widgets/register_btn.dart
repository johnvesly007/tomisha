import 'package:flutter/material.dart';
import 'package:tomisha_work/utils/app_size.dart';
import 'package:tomisha_work/utils/color_constants.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtils.getResponsiveHeight(
          context: context, portionHeightValue: 40),
      decoration: BoxDecoration(
        gradient: const LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [AppColor.commonGreen, AppColor.linearBlue]),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
          bottomLeft: Radius.circular(12.0),
          bottomRight: Radius.circular(12.0),
        ),
        boxShadow: [
          BoxShadow(
            color: AppColor.borderColor.withOpacity(0.3), // Shadow color
            spreadRadius: 0,
            blurRadius: 6,
            offset: const Offset(0, -3),
          ),
        ],
      ),
      child: const Row(
        children: [
          Expanded(
            child: Center(
              child: Text(
                "Kostenlos Registrieren",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 14,
                  color: AppColor.lightBlue,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Lato",
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
