import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tomisha_work/screen/widgets/register_btn.dart';
import 'package:tomisha_work/utils/clipper.dart';
import 'package:tomisha_work/utils/app_size.dart';
import 'package:tomisha_work/utils/color_constants.dart';
import 'package:flutter_svg/svg.dart';

enum Screen {
  Arbeitnehmer,
  Arbeitgeber,
  Temporarburo,
}

class HeaderTitle extends StatelessWidget {
  final String title;
  const HeaderTitle({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ScreenUtils.widthSpace(20, context),
        Container(
          width: ScreenUtils.getResponsiveWidth(
              context: context, portionWidthValue: 320),
          height: ScreenUtils.getResponsiveHeight(
              context: context, portionHeightValue: 100),
          child: Center(
              child: Text(
            title,
            style: const TextStyle(
                fontFamily: "Lato", fontSize: 42, fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          )),
        ),
        ScreenUtils.widthSpace(20, context)
      ],
    );
  }
}

class ScrollingContent extends StatefulWidget {
  const ScrollingContent({
    super.key,
  });

  @override
  State<ScrollingContent> createState() => _ScrollingContentState();
}

class _ScrollingContentState extends State<ScrollingContent> {
  Screen selectedScreen = Screen.Arbeitnehmer;

  void updateScreen(Screen screen) {
    setState(() {
      selectedScreen = screen;
    });
  }

  String getAppropriateString(
      {required String textArbeitnehmer,
      required String textArbeitgener,
      required String textTemporarburo}) {
    return selectedScreen == Screen.Arbeitnehmer
        ? textArbeitnehmer
        : selectedScreen == Screen.Arbeitgeber
            ? textArbeitgener
            : selectedScreen == Screen.Temporarburo
                ? textTemporarburo
                : "";
  }

  @override
  Widget build(BuildContext context) {
    bool isMobile = ScreenUtils.screenWidth(context) < 600;
    bool isWeb = ScreenUtils.screenWidth(context) > 600;

    return Expanded(
        child: SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            color: Colors.white,
            child: Column(
              children: [
                ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 587.37),
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)]),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 461.22),
                ),
                Stack(
                  children: [
                    ClipPath(
                      clipper: ThirdWaveClipper(),
                      child: Container(
                        height: ScreenUtils.getResponsiveHeight(
                            context: context, portionHeightValue: 370),
                        width: double.infinity,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xFFE6FFFA),
                              Color(0xFFEBF4FF),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                        child: ClipPath(
                      clipper: SecondWaveClipper(),
                      child: Container(
                        height: ScreenUtils.getResponsiveHeight(
                            context: context, portionHeightValue: 30),
                        width: double.infinity,
                        color: Colors.white,
                      ),
                    ))
                  ],
                ),
              ],
            ),
          ),
          isMobile
              ? Column(
                  children: [
                    ScreenUtils.heightSpace(18, context),
                    const HeaderTitle(
                      title: "Deine Job \nwebsite",
                    ),
                    ScreenUtils.heightSpace(2, context),
                    Container(
                      height: ScreenUtils.getResponsiveHeight(
                          context: context, portionHeightValue: 404.84),
                      width: double.infinity,
                      child: SvgPicture.asset(
                        "assets/undraw_agreement_aajr.svg",
                      ),
                    ),
                    ScreenUtils.heightSpace(114.16, context),
                    ToggleTabBar(
                      updateScreen: updateScreen,
                      selected: selectedScreen,
                    ),
                    ScreenUtils.heightSpace(30, context),
                    Text(
                      getAppropriateString(
                          textArbeitnehmer:
                              "Drei einfache Schritte\nzu deinem neuen Job",
                          textArbeitgener:
                              "Drei einfache Schritte\n zu deinem neuen Mitarbeiter",
                          textTemporarburo:
                              "Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter"),
                      style: const TextStyle(
                          fontFamily: "Lato",
                          color: AppColor.blackText,
                          fontSize: 21,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    ),
                    ScreenUtils.heightSpace(20.6, context),
                    sectionOne(context, selectedScreen),
                    sectionTwo(context, selectedScreen),
                    ScreenUtils.heightSpace(13.36, context),
                    sectionThree(context, selectedScreen),
                    ScreenUtils.heightSpace(137.21, context),
                  ],
                )
              : isWeb
                  ? Column(
                      children: [
                        Row(
                          children: [
                            Expanded(child: Container()),
                            Expanded(
                                flex: 3,
                                child: Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Text(
                                          'Deine Job \nwebsite',
                                          style: TextStyle(
                                              fontFamily: "Lato",
                                              fontSize: 42,
                                              fontWeight: FontWeight.w500),
                                          textAlign: TextAlign.start,
                                        ),
                                        Container(
                                          height: 20,
                                        ),
                                        SizedBox(
                                            width: ScreenUtils.screenWidth(
                                                    context) /
                                                5,
                                            child: const RegisterButton())
                                      ],
                                    ),
                                    Expanded(
                                      child: Column(
                                        children: [
                                          ScreenUtils.heightSpace(50, context),
                                          Container(
                                            width:
                                                ScreenUtils.getResponsiveHeight(
                                                    context: context,
                                                    portionHeightValue: 455),
                                            height:
                                                ScreenUtils.getResponsiveHeight(
                                                    context: context,
                                                    portionHeightValue: 455),
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white,
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'home_handShake.png')),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                            Expanded(child: Container()),
                          ],
                        ),
                        ScreenUtils.heightSpace(114.16, context),
                        ToggleTabBar(
                          updateScreen: updateScreen,
                          selected: selectedScreen,
                        ),
                        ScreenUtils.heightSpace(30, context),
                        Text(
                          getAppropriateString(
                              textArbeitnehmer:
                                  "Drei einfache Schritte\nzu deinem neuen Job",
                              textArbeitgener:
                                  "Drei einfache Schritte\n zu deinem neuen Mitarbeiter",
                              textTemporarburo:
                                  "Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter"),
                          style: const TextStyle(
                              fontFamily: "Lato",
                              color: AppColor.blackText,
                              fontSize: 21,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                        ),
                        ScreenUtils.heightSpace(20.6, context),
                        sectionOne(context, selectedScreen),
                        sectionTwo(context, selectedScreen),
                        ScreenUtils.heightSpace(13.36, context),
                        sectionThree(context, selectedScreen),
                        ScreenUtils.heightSpace(137.21, context),
                      ],
                    )
                  : Container(),
        ],
      ),
    ));
  }

  Widget sectionOne(BuildContext context, Screen screen) {
    return Stack(
      children: [
        Row(
          children: [
            Stack(
              children: [
                Container(
                  // Parent Container
                  width: ScreenUtils.getResponsiveWidth(
                      context: context, portionWidthValue: 321),
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 264.94),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 219.56),
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 144.55),
                    child: SvgPicture.asset(
                      "assets/undraw_Profile_data_re_v81r.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(child: Container()),
          ],
        ),
        Positioned(
          left: 24,
          bottom: 50,
          child: Container(
            // width: 122,
            height: 156,
            child: Row(
              children: [
                const Text(
                  '1.',
                  style: TextStyle(
                      fontSize: 130,
                      fontFamily: "Lato",
                      color: AppColor.greyText),
                ),
                ScreenUtils.widthSpace(23, context),
              ],
            ),
          ),
        ),
        Positioned(
            right: 39,
            bottom: 50,
            child: Container(
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 55),
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 184),
              // color: Colors.pink,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // Container(),
                  Text(
                    getAppropriateString(
                        textArbeitnehmer: 'Erstellen dein Lebenslauf',
                        textArbeitgener: 'Erstellen dein \nUnternehmensprofil',
                        textTemporarburo:
                            'Erstellen dein \nUnternehmensprofil'),
                    style: const TextStyle(
                        fontSize: 19,
                        fontFamily: "Lato",
                        color: AppColor.greyText,
                        letterSpacing: 0.47,
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ))
      ],
    );
  }

  Widget sectionTwo(BuildContext context, Screen screen) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 37),
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 156),
              // color: Colors.purple,
            ),
            Container(
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 102),
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 156),
              // color: Colors.yellow,
              child: const Text(
                '2.',
                style: TextStyle(
                    fontSize: 130,
                    fontFamily: "Lato",
                    color: AppColor.greyText),
              ),
            ),
            Expanded(
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(child: Container()),
                  Row(
                    children: [
                      Container(
                        width: ScreenUtils.getResponsiveWidth(
                            context: context, portionWidthValue: 207),
                        height: ScreenUtils.getResponsiveHeight(
                            context: context, portionHeightValue: 156),
                        // color: Colors.yellow,
                        child: Column(
                          children: [
                            ScreenUtils.heightSpace(112, context),
                            Expanded(
                              child: Text(
                                // 'Erstellen dein Lebenslauf',
                                getAppropriateString(
                                    textArbeitnehmer:
                                        'Erstellen dein Lebenslauf',
                                    textArbeitgener: 'Erstellen ein Jobinserat',
                                    textTemporarburo:
                                        'Erhalte Vermittlungs- \nangebot von Arbeitgeber'),
                                style: const TextStyle(
                                    fontSize: 19,
                                    fontFamily: "Lato",
                                    color: AppColor.greyText),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // ScreenUtils.widthSpace(19, context)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        Row(
          children: [
            ScreenUtils.widthSpace(112.26, context),
            Expanded(
                child: Container(
              width: ScreenUtils.getResponsiveWidth(
                  context: context,
                  portionWidthValue:
                      screen == Screen.Arbeitgeber ? 28.7 : 180.74),
              height: ScreenUtils.getResponsiveHeight(
                  context: context,
                  portionHeightValue:
                      screen == Screen.Arbeitgeber ? 179.2 : 126.55),
              child: SvgPicture.asset(
                getAppropriateString(
                    textArbeitnehmer: 'assets/undraw_task_31wc.svg',
                    textArbeitgener: 'assets/arbeitgeber_2.svg',
                    textTemporarburo: 'assets/temporarb_2.svg'),
                fit: BoxFit.fill,
              ),
            )),
            ScreenUtils.widthSpace(67, context),
          ],
        )
      ],
    );
  }

  Widget sectionThree(BuildContext context, Screen screen) {
    return Row(
      children: [
        Container(
          height: ScreenUtils.getResponsiveHeight(
              context: context, portionHeightValue: 326.79),
          width: ScreenUtils.getResponsiveWidth(
              context: context, portionWidthValue: 56),
        ),
        Expanded(
          child: Container(
            width: ScreenUtils.getResponsiveWidth(
                context: context, portionWidthValue: 297),
            height: ScreenUtils.getResponsiveHeight(
                context: context, portionHeightValue: 326.79),
            child: Stack(
              children: [
                Stack(
                  children: [
                    Positioned(
                        top: 0,
                        left: 0,
                        child: Container(
                          width: ScreenUtils.getResponsiveWidth(
                              context: context, portionWidthValue: 102),
                          height: ScreenUtils.getResponsiveHeight(
                              context: context, portionHeightValue: 156),
                          child: const Text(
                            '3.',
                            style: TextStyle(
                                fontSize: 130,
                                fontFamily: "Lato",
                                color: AppColor.greyText),
                          ),
                        )),
                    Positioned(
                        top: 0,
                        right: 0,
                        child: Container(
                          width: ScreenUtils.getResponsiveWidth(
                              context: context, portionWidthValue: 195),
                          height: ScreenUtils.getResponsiveHeight(
                              context: context, portionHeightValue: 156),
                          child: Row(
                            children: [
                              ScreenUtils.widthSpace(23, context),
                              Expanded(
                                child: Text(
                                  // 'Mit nur einem Klick bewerben',
                                  getAppropriateString(
                                      textArbeitnehmer:
                                          'Mit nur einem Klick bewerben',
                                      textArbeitgener:
                                          'Wähle deinen \nneuen Mitarbeiter aus',
                                      textTemporarburo:
                                          'Vermittlung nach\n Provision oder \nStundenlohn'),
                                  style: const TextStyle(
                                      fontSize: 19,
                                      fontFamily: "Lato",
                                      color: AppColor.greyText),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        )),
                    Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          width: ScreenUtils.getResponsiveWidth(
                              context: context, portionWidthValue: 281),
                          height: ScreenUtils.getResponsiveHeight(
                              context: context,
                              portionHeightValue: screen != Screen.Arbeitnehmer
                                  ? 190.44
                                  : 210.44),
                          child: SvgPicture.asset(
                            getAppropriateString(
                                textArbeitnehmer:
                                    'assets/undraw_personal_file_222m.svg',
                                textArbeitgener: 'assets/arbeitgeber_3.svg',
                                textTemporarburo: 'assets/temporarb_3.svg'),
                            fit: BoxFit.fill,
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
        ),
        ScreenUtils.widthSpace(7, context)
      ],
    );
  }
}

class ToggleTabBar extends StatefulWidget {
  final Function updateScreen;
  final Screen selected;
  const ToggleTabBar(
      {super.key, required this.updateScreen, required this.selected});

  @override
  State<ToggleTabBar> createState() => _ToggleTabBarState();
}

class _ToggleTabBarState extends State<ToggleTabBar> {
  int toggleCount = 0;
  @override
  Widget build(BuildContext context) {
    return ScreenUtils.screenWidth(context) < 600
        ? widget.selected == Screen.Arbeitnehmer
            ? buildArbeitnehmerToggle(context, widget.selected)
            : widget.selected == Screen.Arbeitgeber
                ? buildArbeitgeberToggle(context, widget.selected)
                : widget.selected == Screen.Temporarburo
                    ? buildTemporarburoToggle(context, widget.selected)
                    : Container()
        : buildWebToggle(context, widget.selected);
  }

  Widget buildArbeitnehmerToggle(BuildContext context, dynamic selected) {
    return Row(
      children: [
        ScreenUtils.widthSpace(20, context),
        Expanded(
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    toggleCount = 0;
                    // selected = Screen.Arbeitnehmer;
                    widget.updateScreen(Screen.Arbeitnehmer);
                  });
                },
                child: Container(
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 40),
                  width: ScreenUtils.getResponsiveWidth(
                      context: context, portionWidthValue: 159.5),
                  decoration: BoxDecoration(
                    color: toggleCount == 0
                        ? AppColor.filltoggleButton
                        : Colors.white,
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(12.0),
                      bottomLeft: Radius.circular(12.0),
                    ),
                    border: Border.all(color: AppColor.toggleBorderColor),
                  ),
                  child: Center(
                    child: Text(
                      'Arbeitnehmer',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Lato",
                          color: toggleCount == 0
                              ? AppColor.white
                              : AppColor.toggleUnSelectedFontColor,
                          letterSpacing: 0.47,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    toggleCount = 1;
                    // selected = Screen.Arbeitgeber;
                    widget.updateScreen(Screen.Arbeitgeber);
                  });
                },
                child: Container(
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 40),
                  width: ScreenUtils.getResponsiveWidth(
                      context: context, portionWidthValue: 159.5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: AppColor.toggleBorderColor)),
                  child: Center(
                    child: Text(
                      'Arbeitgeber',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Lato",
                          color: toggleCount == 1
                              ? AppColor.white
                              : AppColor.toggleUnSelectedFontColor,
                          letterSpacing: 0.47,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      toggleCount = 2;
                      // selected = Screen.Temporarburo;
                      widget.updateScreen(Screen.Temporarburo);
                    });
                  },
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 40),
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 159.5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: const BorderRadius.only(
                            // topLeft: Radius.circular(12.0),
                            // topRight: Radius.circular(12.0),
                            // bottomLeft: Radius.circular(12.0),
                            // bottomRight: Radius.circular(12.0),
                            ),
                        border: Border.all(color: AppColor.toggleBorderColor)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildArbeitgeberToggle(BuildContext context, dynamic selected) {
    return Row(
      children: [
        Expanded(
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    toggleCount = 0;
                    widget.updateScreen(Screen.Arbeitnehmer);
                  });
                },
                child: Container(
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 40),
                  width: ScreenUtils.getResponsiveWidth(
                      context: context, portionWidthValue: 99.75),
                  decoration: BoxDecoration(
                    color: toggleCount == 0
                        ? AppColor.filltoggleButton
                        : Colors.white,
                    border: Border.all(color: AppColor.toggleBorderColor),
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'eitnehmer',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Lato",
                          color: toggleCount == 0
                              ? AppColor.white
                              : AppColor.toggleUnSelectedFontColor,
                          letterSpacing: 0.47,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    toggleCount = 1;
                    widget.updateScreen(Screen.Arbeitgeber);
                  });
                },
                child: Container(
                  height: ScreenUtils.getResponsiveHeight(
                      context: context, portionHeightValue: 40),
                  width: ScreenUtils.getResponsiveWidth(
                      context: context, portionWidthValue: 159.5),
                  decoration: BoxDecoration(
                      color: toggleCount == 1
                          ? AppColor.filltoggleButton
                          : Colors.white,
                      border: Border.all(color: AppColor.toggleBorderColor)),
                  child: Center(
                    child: Text(
                      'Arbeitgeber',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Lato",
                          color: toggleCount == 1
                              ? AppColor.white
                              : AppColor.toggleUnSelectedFontColor,
                          letterSpacing: 0.47,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      toggleCount = 2;
                      widget.updateScreen(Screen.Temporarburo);
                    });
                  },
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 40),
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 159.5),
                    decoration: BoxDecoration(
                        color: toggleCount == 2
                            ? AppColor.toggleUnSelectedFontColor
                            : AppColor.white,
                        border: Border.all(color: AppColor.toggleBorderColor)),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Temporärb',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Lato",
                            color: toggleCount == 2
                                ? AppColor.white
                                : AppColor.toggleUnSelectedFontColor,
                            letterSpacing: 0.47,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildWebToggle(BuildContext context, Screen selected) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              setState(() {
                toggleCount = 0;
                widget.updateScreen(Screen.Arbeitnehmer);
              });
            },
            child: Container(
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 40),
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 60),
              decoration: BoxDecoration(
                  color: toggleCount == 0
                      ? AppColor.filltoggleButton
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    // topRight: Radius.circular(12.0),
                    bottomLeft: Radius.circular(12.0),
                    // bottomRight: Radius.circular(12.0),
                  ),
                  border: Border.all(color: AppColor.toggleBorderColor)),
              child: Center(
                child: Text(
                  'Arbeitnehmer',
                  style: TextStyle(
                      fontSize: 14,
                      fontFamily: "Lato",
                      color: toggleCount == 0
                          ? AppColor.white
                          : AppColor.toggleUnSelectedFontColor,
                      letterSpacing: 0.47,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                toggleCount = 1;
                widget.updateScreen(Screen.Arbeitgeber);
              });
            },
            child: Container(
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 40),
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 60),
              decoration: BoxDecoration(
                  color: toggleCount == 1
                      ? AppColor.filltoggleButton
                      : Colors.white,
                  border: Border.all(color: AppColor.toggleBorderColor)),
              child: Center(
                child: Text(
                  'Arbeitgeber',
                  style: TextStyle(
                      fontSize: 14,
                      fontFamily: "Lato",
                      color: toggleCount == 1
                          ? AppColor.white
                          : AppColor.toggleUnSelectedFontColor,
                      letterSpacing: 0.47,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                toggleCount = 2;
                widget.updateScreen(Screen.Temporarburo);
              });
            },
            child: Container(
              height: ScreenUtils.getResponsiveHeight(
                  context: context, portionHeightValue: 40),
              width: ScreenUtils.getResponsiveWidth(
                  context: context, portionWidthValue: 60),
              decoration: BoxDecoration(
                  color: toggleCount == 2
                      ? AppColor.filltoggleButton
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    // topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                    // bottomLeft: Radius.circular(12.0),
                    bottomRight: Radius.circular(12.0),
                  ),
                  border: Border.all(color: AppColor.toggleBorderColor)),
              child: Center(
                child: Text(
                  'Temporärbüro',
                  style: TextStyle(
                      fontSize: 14,
                      fontFamily: "Lato",
                      color: toggleCount == 2
                          ? AppColor.white
                          : AppColor.toggleUnSelectedFontColor,
                      letterSpacing: 0.47,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTemporarburoToggle(BuildContext context, Screen selected) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      toggleCount = 0;
                      widget.updateScreen(Screen.Arbeitnehmer);
                    });
                  },
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 40),
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 19.5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: AppColor.toggleBorderColor)),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      toggleCount = 1;
                      widget.updateScreen(Screen.Arbeitgeber);
                    });
                  },
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 40),
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 160),
                    decoration: BoxDecoration(
                      color: toggleCount == 1
                          ? AppColor.filltoggleButton
                          : Colors.white,
                      border: Border.all(color: AppColor.toggleBorderColor),
                    ),
                    child: Center(
                      child: Text(
                        'Arbeitgeber',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Lato",
                            color: toggleCount == 1
                                ? AppColor.white
                                : AppColor.toggleUnSelectedFontColor,
                            letterSpacing: 0.47,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      toggleCount = 2;
                      widget.updateScreen(Screen.Temporarburo);
                    });
                  },
                  child: Container(
                    height: ScreenUtils.getResponsiveHeight(
                        context: context, portionHeightValue: 40),
                    width: ScreenUtils.getResponsiveWidth(
                        context: context, portionWidthValue: 159.5),
                    decoration: BoxDecoration(
                        color: toggleCount == 2
                            ? AppColor.filltoggleButton
                            : Colors.white,
                        borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(12.0),
                          bottomRight: Radius.circular(12.0),
                        ),
                        border: Border.all(color: AppColor.toggleBorderColor)),
                    child: Center(
                      child: Text(
                        'Temporärbüro',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Lato",
                            color: toggleCount == 2
                                ? AppColor.white
                                : AppColor.toggleUnSelectedFontColor,
                            letterSpacing: 0.47,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
