import 'package:flutter/material.dart';

class ScreenUtils {
  static double screenHeight(BuildContext context) =>
      MediaQuery.of(context).size.height;

  static double screenWidth(BuildContext context) =>
      MediaQuery.of(context).size.width;

  static double adobeXdHeight = 844;
  static double adobeXdWidth = 360;

  static double getResponsiveHeight({
    required BuildContext context,
    required double portionHeightValue,
  }) {
    final availableHeight = ScreenUtils.screenHeight(context);
    double heightFactor = portionHeightValue / adobeXdHeight;
    double height = availableHeight * heightFactor;
    return height;
  }

  static double getResponsiveWidth({
    required BuildContext context,
    required double portionWidthValue,
  }) {
    final screenWidth = ScreenUtils.screenWidth(context);
    double widthFactor = portionWidthValue / adobeXdWidth;
    double width = screenWidth * widthFactor;
    return width;
  }
   static Widget widthSpace(double value, context) {
    return SizedBox(
      width: ScreenUtils.getResponsiveWidth(
          context: context, portionWidthValue: value),
    );
  }
  static Widget heightSpace(double value, context) {
    return SizedBox(
      height: ScreenUtils.getResponsiveHeight(
          context: context, portionHeightValue: value),
    );
  }
}

