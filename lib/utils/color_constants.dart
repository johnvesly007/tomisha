import 'package:flutter/material.dart';

class AppColor {
  static const Color commonGreen = Color(0xFF319795);
  static const Color linearBlue = Color(0xFF3182CE);
  static const Color lightBlue = Color(0xFFE6FFFA);

  static const Color shadowColor = Color(0x00000029);
  static const Color borderColor = Color(0xFF707070);
  static const Color blackText = Color(0xFF4A5568);
  static const Color greyText = Color(0xFF718096);

  static const Color toggleBorderColor = Color(0xFFCBD5E0);
  static const Color filltoggleButton = Color(0xFF81E6D9);
  static const Color toggleSelectedFontColor = Color(0xFFE6FFFA);
  static const Color toggleUnSelectedFontColor = Color(0xFF319795);

  static const Color white = Color(0xFFFFFFFF);
}
